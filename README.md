# User Information Tool
Tool for querying Grainger Identity Vault LDAP user information and Active Directory user groups.

This tool is written C# for .Net 5 or better.  And can be compiled for Windows, Mac and Linux targets.

The tool has an ANSI (XTerm/Curses) graphical interface that can be invoked with the `--interactive` option.

More information and distribution links for Windows, Linux and Mac can be found in Confluence.

* #### [Confluence Page and Binaries](https://confluence.grainger.com/display/~mrtlb/UserInfo+Tool)

### Example

#### Command line help
```bash
❯ ./userinfo-tool --help
userinfo-tool

Usage:
  userinfo-tool [options] [<args>...]

Arguments:
  <args>  List of users (racfids) to check

Options:
  --interactive   Show ANSI UI
  --ldap-info     Display LDAP (Identity Vault) info for the users
  --user-groups   Display AD groups the users belong to
  --group-detail  Display details for a specific group, including members
  --find-user     Search IDV for users matching strings provided
  --version       Show version information
  -?, -h, --help  Show help and usage information

```

