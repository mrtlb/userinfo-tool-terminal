using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using userinfo_tool.Annotations;
using userinfo_tool.Models;
using userinfo_tool.Services;

namespace userinfo_tool.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private readonly Credentials _credentials;
        private readonly DirectoryServices _directory;

        private string _password;
        private string _userName;

        public LoginViewModel(DirectoryServices directory)
        {
            _credentials = directory.Credentials;
            _directory = directory;
            _userName = _credentials.UserName;
            _password = _credentials.Password;
        }

        public string UserName
        {
            get => _userName;
            set
            {
                if (_userName == value) return;
                _userName = value;
                _credentials.UserName = _userName;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (_password == value) return;
                _password = value;
                _credentials.Password = _password;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public async Task<bool> Authenticate()
        {
            return await _directory.AuthenticateAsync();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}