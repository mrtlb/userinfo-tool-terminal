using System.Collections.Generic;
using userinfo_tool.Models;

namespace userinfo_tool.ViewModels
{
    public class FoundUsersViewModel
    {
        public FoundUsersViewModel(IEnumerable<UserName> userNames)
        {
            UserNames = userNames;
            SelectedUserNames = new List<UserName>();
        }

        public IEnumerable<UserName> UserNames { get; }
        public List<UserName> SelectedUserNames { get; }
        public bool Cancelled { get; set; }
    }
}