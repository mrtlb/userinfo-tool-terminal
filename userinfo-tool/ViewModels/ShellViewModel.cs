using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Novell.Directory.Ldap;
using userinfo_tool.Models;
using userinfo_tool.Services;

namespace userinfo_tool.ViewModels
{
    public class ShellViewModel
    {
        private readonly Dictionary<string, Dictionary<string, string>> _cachedDetails =
            new(StringComparer.CurrentCultureIgnoreCase);

        private readonly Dictionary<string, Dictionary<string, string>> _cachedUserGroups =
            new(StringComparer.CurrentCultureIgnoreCase);

        private readonly DirectoryServices _directory;

        private string _lastLoggedInUserName;

        public ShellViewModel()
        {
            _directory = new DirectoryServices();
        }

        public async Task<List<UserName>> ResolveNames(string namesString)
        {
            var userList = new List<UserName>();
            if (string.IsNullOrWhiteSpace(namesString)) return userList;
            
            using (var cnn = await _directory.GetIDVConnectionAsync())
            {
                var result = _directory.FindIDVUsersAsync(cnn, namesString?.Split(','));
                await foreach (var entry in result)
                    userList.Add(new UserName
                    {
                        Uid = entry.GetAttribute("uid").StringValue,
                        DistinguishedName = entry.Dn,
                        FirstName = entry.GetAttribute("givenName").StringValue,
                        LastName = entry.GetAttribute("sn").StringValue,
                        FullName = entry.GetAttribute("fullName").StringValue
                    });
            }

            return userList;
        }

        public async Task<Dictionary<string, string>> GetLdapDetail(string userId)
        {
            if (_cachedDetails.ContainsKey(userId)) return _cachedDetails[userId];

            using (var cnn = await _directory.GetIDVConnectionAsync())
            {
                var result = await _directory.SearchIDVUserAsync(cnn, userId);
                if (result != null)
                {
                    var entry = await result.SingleOrDefaultAsync();
                    if (entry != null) _cachedDetails.TryAdd(userId, ConvertLdapDetail(entry));
                }
            }

            return _cachedDetails.TryGetValue(userId, out var found) ? found : null;
        }

        private Dictionary<string, string> ConvertLdapDetail(LdapEntry entry)
        {
            return entry.GetAttributeSet().ToDictionary(kvp => kvp.Key,
                kvp => string.Join("\n\t\t\t", string.Join('\n', kvp.Value.StringValueArray)));
        }

        public async IAsyncEnumerable<UserGroupMatch> GetUserGroups(IEnumerable<string> nameUids,
            Action<string> message)
        {
            //User groups are per domain, an new user login could indicate a new domain
            if (_lastLoggedInUserName != Credentials.Default.UserName) _cachedUserGroups.Clear();
            _lastLoggedInUserName = Credentials.Default.UserName;

            using (var cnn = await _directory.GetActiveDirectoryConnectionAsync(message))
            {
                // Get all the user groups
                var userDict = new Dictionary<string, Dictionary<string, string>>();
                foreach (var user in nameUids)
                {
                    if (_cachedUserGroups.ContainsKey(user)) continue;
                    _cachedUserGroups.Add(user, await _directory.GetUserGroupsAsync(cnn, user));
                }

                // Now, pivot this around to groups, and add users
                var qry =
                    from u in _cachedUserGroups
                    from g in u.Value
                    where nameUids.Contains(u.Key, StringComparer.CurrentCultureIgnoreCase)
                    orderby u.Key, g.Key
                    select new { User = u.Key, Group = g.Key, GroupDn = g.Value };

                var groups = from r in qry
                    group r by r.Group
                    into g
                    orderby g.Count() descending
                    select new { Group = g.Key, g.First().GroupDn, Users = g.Select(r => r.User) };

                var groupList = groups.ToList();
                foreach (var ug in groupList)
                    yield return new UserGroupMatch
                        { Name = ug.Group, Dn = ug.GroupDn, MarkedUsers = ug.Users.ToList() };
            }
        }

        public LoginViewModel GetLoginViewModel()
        {
            return new LoginViewModel(_directory);
        }

        public async Task<UserGroupDetails> GetGroupDetail(string groupName, Action<string> message)
        {
            using (var cnn = await _directory.GetActiveDirectoryConnectionAsync(message))
            {
                return await _directory.SearchActiveDirectoryGroupAsync(cnn, groupName, message);
            }
        }
    }
}