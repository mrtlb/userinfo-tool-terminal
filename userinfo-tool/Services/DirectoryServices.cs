using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using DnsClient;
using DnsClient.Protocol;
using Novell.Directory.Ldap;
using userinfo_tool.Code;
using userinfo_tool.Models;

namespace userinfo_tool.Services
{
    public class DirectoryServices
    {
        private const string BaseDn = "ou=users,o=Grainger";
        private const string AppUserDn = "cn=zTBAccess,ou=srv,ou=chi,o=grainger";
        private const string AppUserPw = "jeAnSp10uF6Szs79cW0Ujuz2zZqAXEK70axFBWZOIHfDjjs5ZkOou7oSGqXzkt58";
        private const string IdvLdapHost = "pridv01.lf.grainger.com";
        private const string ActiveDirectoryPrefix = "_ldap._tcp.";
        private const string DefaultDomainSuffix = ".grainger.com";
        private const int IdvLdapPort = 636;

        private readonly LdapConstraints _ldapBindConstraints = new(2000, true, null, 2);

        private readonly LdapSearchConstraints _ldapSearchConstraints =
            new(2000, 2, LdapSearchConstraints.DerefNever, 1000, true, 1, null, 2);

        public Credentials Credentials => Credentials.Default;

        public async Task<bool> AuthenticateAsync()
        {
            try
            {
                using (var cnn = await GetIDVConnectionAsync())
                {
                    var parts = Credentials.UserName?.Split("\\");
                    var constraint = _ldapBindConstraints;
                    await cnn.BindAsync(LdapConnection.LdapV3, $"cn={parts?.Last()},ou=users,o=Grainger",
                        Credentials.Password, constraint);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public async Task<LdapConnection> GetIDVConnectionAsync()
        {
            //var encrypted = EncryptionUtils.EncryptString(AppUserPw);
            var opt = new LdapConnectionOptions()
                .ConfigureRemoteCertificateValidationCallback((sender, certificate, chain, errors) => true)
                .UseSsl();
            var ldap = new LdapConnection(opt);
            await ldap.ConnectAsync(IdvLdapHost, IdvLdapPort);
            await ldap.BindAsync(LdapConnection.LdapV3, AppUserDn, EncryptionUtils.DecryptToString(AppUserPw),
                _ldapBindConstraints);
            //await ldap.BindAsync(LdapConnection.LdapV3, $"cn=****,ou=users,o=Grainger", "***");
            return ldap;
        }

        public async Task<ILdapSearchResults> SearchIDVUserAsync(LdapConnection cnn, string user)
        {
            return await cnn.SearchAsync(BaseDn, LdapConnection.ScopeOne,
                $"(|(&(objectClass=wwgForeignUser)(wwgForeignUserId={user}))(uid={user}))", null, false,
                _ldapSearchConstraints);
        }

        /// <summary>
        ///     Find a subset of information (uid, givenName, sn, fullName) for a list of users in IDV
        /// </summary>
        /// <param name="cnn"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        public async Task<ILdapSearchResults> SearchIDVUserSummaryAsync(LdapConnection cnn, string[] users)
        {
            if (users == null || users.Length == 0) return null;
            var queryString =
                string.Concat(users.Select(u => $"(|(&(objectClass=wwgForeignUser)(wwgForeignUserId={u}))(uid={u}))"));
            if (users.Length > 1)
                queryString = $"(|{queryString})";
            return await cnn.SearchAsync(BaseDn, LdapConnection.ScopeOne,
                queryString, new[] { "uid", "givenName", "sn", "fullName" }, false, _ldapSearchConstraints);
        }

        /// <summary>
        ///     Resolve names of Identity Vault users
        /// </summary>
        /// <param name="cnn"></param>
        /// <param name="terms"></param>
        /// <returns></returns>
        public async IAsyncEnumerable<LdapEntry> FindIDVUsersAsync(LdapConnection cnn, string[] terms)
        {
            if (terms == null || terms.Length == 0) yield break;
            const string criteria = "(&(|(|(&(objectClass=wwgForeignUser)(wwgForeignUserId={0}))(uid={0}))" +
                                    "(|(&(sn={0}*)(givenName={1}*))(&(sn={1}*)(givenName={0}*))))(employeeStatus=A))";
            var query = string.Concat(terms.Where(r=>!string.IsNullOrWhiteSpace(r)).Select(t =>
                string.Format(criteria,
                    t.Split(' ',
                            StringSplitOptions.RemoveEmptyEntries)
                        .Concat(new[]
                        {
                            ""
                        })
                        .Cast<object>()
                        .ToArray())));
            if (terms.Length > 1) query = $"(|{query})";
            var result = await cnn.SearchAsync(BaseDn, LdapConnection.ScopeOne,
                query,
                new[] { "uid", "givenName", "sn", "fullName" },
                false, _ldapSearchConstraints);
            if (result != null)
                await foreach (var entry in result)
                    yield return entry;
        }

        public async Task<LdapConnection> GetActiveDirectoryConnectionAsync(Action<string> message)
        {
            if (string.IsNullOrWhiteSpace(Credentials.UserName) || string.IsNullOrWhiteSpace(Credentials.Password))
                throw new AuthenticationException("Invalid/empty credentials provided");

            var domain = GetLoginDomain();

            // Find a domain controller
            var dns = new LookupClient();
            var qry = await dns.QueryAsync($"{ActiveDirectoryPrefix}{domain}", QueryType.SRV);
            var ldapServers = qry.Answers.Cast<SrvRecord>().Select(s => s.Target.Value.TrimEnd('.')).ToList();

#if NET5_0
            var rnd = new Random();
#else
            // .Net 6 has a thread-safe shared Random, so no need to make one.
            var rnd = Random.Shared;
#endif

            // Connect to the domain controllers as LDAP
            var opt = new LdapConnectionOptions()
                .ConfigureRemoteCertificateValidationCallback((sender, certificate, chain, errors) => true)
                .UseSsl();
            var activeDirectoryConnection = new LdapConnection(opt);
            while (!activeDirectoryConnection.Connected && ldapServers.Count > 0)
            {
                var idx = rnd.Next(ldapServers.Count);
                try
                {
                    await activeDirectoryConnection.ConnectAsync(ldapServers[idx], IdvLdapPort);
                    message($"Connected to AD: {ldapServers[idx]}\n\n");
                }
                catch (Exception)
                {
                    message($"Failed AD connect: {ldapServers[idx]}.");
                    ldapServers.RemoveAt(idx);
                }
            }

            await activeDirectoryConnection.BindAsync(LdapConnection.LdapV3, Credentials.UserName,
                Credentials.Password, _ldapBindConstraints);
            return activeDirectoryConnection;
        }

        private string GetLoginDomain()
        {
            var parts = Credentials.UserName?.Split("\\");
            var domain = parts.Length > 1 ? parts[0] : "campus";
            domain += domain.Contains('.') ? "" : DefaultDomainSuffix;
            return domain;
        }

        public async Task<ILdapSearchResults> SearchActiveDirectoryUserAsync(LdapConnection cnn, string user)
        {
            var baseDn = string.Join(',', GetLoginDomain().Split('.').Select(s => $"dc={s}"));
            return await cnn.SearchAsync(baseDn, LdapConnection.ScopeSub,
                $"(&(objectclass=person)(sAMAccountName={user}))",
                new[]
                {
                    "objectClass", "cn", "sn", "givenName", "distinguishedName", "displayName", "memberOf", "name",
                    "userPrincipalName", "sAMAccountName"
                },
                false,
                _ldapSearchConstraints);
        }

        public async Task<Dictionary<string, string>> GetUserGroupsAsync(LdapConnection adCnn, string user)
        {
            var results = await SearchActiveDirectoryUserAsync(adCnn, user);

            var entry = await results.FirstOrDefaultAsync();
            return entry == null
                ? new Dictionary<string, string>()
                : entry.GetAttribute("memberOf").StringValueArray
                    .OrderBy(s => s, StringComparer.CurrentCultureIgnoreCase)
                    .ToDictionary(s => s.Split(',', '@').First()[3..], s => s);
        }

        public async Task<UserGroupDetails> SearchActiveDirectoryGroupAsync(LdapConnection adCnn, string groupName,
            Action<string> message)
        {
            var baseDn = string.Join(',', GetLoginDomain().Split('.').Select(s => $"dc={s}"));

            var results = await adCnn.SearchAsync(baseDn, LdapConnection.ScopeSub,
                $"(&(objectclass=group)(CN={groupName}*))",
                new[] { "cn", "member", "distinguishedName", "name" /*, "description"*/ },
                false,
                _ldapSearchConstraints);

            var entry = await results.FirstOrDefaultAsync();
            if (entry == null) return null;

            return new UserGroupDetails
            {
                Name = entry.GetAttribute("name").StringValue,
                Dn = entry.GetAttribute("distinguishedName").StringValue,
                Description = null,
                Members = await GetFullMembersAsync(adCnn, entry.GetAttribute("member").StringValueArray, message)
                    .ToListAsync(),
                UserDns = entry.GetAttribute("member").StringValueArray
                // Members = entry.GetAttribute("member").StringValueArray.Select(r => new UserName
                // {
                //     Uid = r[3..].Split(',').FirstOrDefault(),
                //     DistinguishedName = r
                // }).ToList()
            };
        }

        public async IAsyncEnumerable<UserName> GetFullMembersAsync(LdapConnection adCnn,
            string[] distinguishedNames, Action<string> message)
        {
            var nameStrings = distinguishedNames.Select(s => s[3..].Split(',', '@').FirstOrDefault());
            message($"Getting full names for: [{string.Join(", ", nameStrings)}]");
            using (var idvCnn = await GetIDVConnectionAsync())
            {
                // var result = FindIDVUsersAsync(idvCnn, nameStrings.ToArray());
                // await foreach (var entry in result)
                //     yield return new UserName
                //     {
                //         Uid = entry.GetAttribute("uid").StringValue,
                //         DistinguishedName = entry.Dn,
                //         FirstName = entry.GetAttribute("givenName").StringValue,
                //         LastName = entry.GetAttribute("sn").StringValue,
                //         FullName = entry.GetAttribute("fullName").StringValue
                //     };

                var result = await SearchIDVUserSummaryAsync(idvCnn, nameStrings.ToArray());
                if (result == null) yield break;

                await foreach (var entry in result)
                    yield return new UserName
                    {
                        Uid = entry.GetAttribute("uid").StringValue,
                        DistinguishedName = entry.Dn,
                        FirstName = entry.GetAttribute("givenName").StringValue,
                        LastName = entry.GetAttribute("sn").StringValue,
                        FullName = entry.GetAttribute("fullName").StringValue
                    };
            }
        }
    }
}