﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Novell.Directory.Ldap;
using Terminal.Gui;
using userinfo_tool.Code;
using userinfo_tool.Models;
using userinfo_tool.Services;
using userinfo_tool.ViewModels;
using userinfo_tool.Views;

namespace userinfo_tool
{
    internal class Program
    {
        /// <summary>
        /// </summary>
        /// <param name="interactive">Show ANSI UI</param>
        /// <param name="ldapInfo">Display LDAP (Identity Vault) info for the users</param>
        /// <param name="userGroups">Display AD groups the users belong to</param>
        /// <param name="groupDetail">Display details for a specific group, including members</param>
        /// <param name="findUser">Search IDV for users matching strings provided</param>
        /// <param name="args">List of users (racfids) to check</param>
        private static async Task Main(bool interactive, bool ldapInfo, bool userGroups, bool groupDetail,
            bool findUser, string[] args)
        {
            if (interactive)
            {
                if (Console.IsInputRedirected)
                {
                    Console.WriteLine("Console input cannot be redirected in interactive mode.");
                    Environment.Exit(2);
                }

                RunInteractive();
                return;
            }

            if (!(ldapInfo || userGroups || findUser || groupDetail))
            {
                Console.WriteLine("Nothing to do. Try \"--help\".");
                Environment.Exit(3);
            }

            var directory = new DirectoryServices();

            // Login
            // NOTE:  Currently only userGroups require an AD login
            if (userGroups || groupDetail) GetUserCredentials();

            if (findUser) await FindIdvUsers(directory, args);

            // Get LDAP Info
            if (ldapInfo) await PrintIdvInfo(directory, args);

            // get User Groups
            if (userGroups) await PrintUserGroups(directory, args);

            if (groupDetail) await PrintGroupDetail(directory, args);
        }

        private static void GetUserCredentials()
        {
            var credentials = Credentials.Default;
            // If this is scripted, don't bother with the fancy key reads and console manipulation, just get the first two strings.
            var redirected = Console.IsInputRedirected;
            if (redirected)
            {
                credentials.UserName = Console.ReadLine();
                credentials.Password = Console.ReadLine();
                Console.WriteLine($"Accepting user {credentials.UserName} from stdin");
            }
            else
            {
                Console.Write("User name (domain\\name):  ");
                credentials.UserName = Console.ReadLine();
                Console.Write("Password:  ");
                credentials.Password = TerminalUtils.GetPassword();
            }
        }

        private static async Task PrintIdvInfo(DirectoryServices directory, string[] users)
        {
            try
            {
                Console.WriteLine($"\n{new string('-', 50)}\n");
                using (var cnn = await directory.GetIDVConnectionAsync())
                {
                    foreach (var user in users)
                    {
                        var results = await directory.SearchIDVUserAsync(cnn, user);

                        var entry = await results.FirstOrDefaultAsync();
                        if (entry == null)
                        {
                            Console.WriteLine($"Entry not found for user [{user}].");
                            Console.WriteLine($"\n{new string('-', 50)}\n");
                            continue;
                        }

                        PrintLdapEntry(user, entry);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    $"An exception occurred getting LDAP Info: \n\n\t{e.ToString().Split('\n').FirstOrDefault()}\n");
            }
        }

        private static async Task FindIdvUsers(DirectoryServices directory, string[] terms)
        {
            Console.WriteLine(
                $"Searching IDV for the following terms: {string.Join(" ", terms.Select(t => $"\"{t}\""))}");
            Console.WriteLine($"\n{new string('-', 50)}\n");

            using (var cnn = await directory.GetIDVConnectionAsync())
            {
                var results = directory.FindIDVUsersAsync(cnn, terms);
                await foreach (var entry in results
                    .OrderBy(e => e.GetAttribute("sn").StringValue, StringComparer.CurrentCultureIgnoreCase)
                    .ThenBy(e => e.GetAttribute("givenName").StringValue, StringComparer.CurrentCultureIgnoreCase)
                    .ThenBy(e => e.Dn, StringComparer.CurrentCultureIgnoreCase))
                    Console.WriteLine(
                        $"{entry.GetAttribute("uid").StringValue}\t\t{entry.GetAttribute("fullName").StringValue,-30}\t{entry.Dn}");
            }

            Console.WriteLine($"\n{new string('-', 50)}\n");
        }

        private static async Task PrintUserGroups(DirectoryServices directory, string[] users)
        {
            try
            {
                Console.WriteLine($"\n{new string('-', 50)}\n");
                using (var cnn = await directory.GetActiveDirectoryConnectionAsync(msg => Console.WriteLine(msg)))
                {
                    // Get all the user groups
                    var userDict = new Dictionary<string, Dictionary<string, string>>();
                    foreach (var user in users) userDict.Add(user, await directory.GetUserGroupsAsync(cnn, user));

                    // Now, pivot this around to groups, and add users
                    var qry =
                        from u in userDict
                        from g in u.Value
                        orderby u.Key, g.Key
                        select new { User = u.Key, Group = g.Key };

                    var groups = from r in qry
                        group r by r.Group
                        into g
                        orderby g.Count() descending
                        select new { Group = g.Key, Users = g.Select(r => r.User) };

                    var groupList = groups.ToList();
                    var grouppad = (int)Math.Ceiling(groupList.Max(r => r.Group.Length + 3) / 10.0) * 10;
                    foreach (var x in groupList)
                        Console.WriteLine($"{x.Group.PadRight(grouppad)}\t {string.Join(", ", x.Users)}");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    $"An exception occurred getting user groups: \n\n\t{e.ToString().Split('\n').FirstOrDefault()}\n");
            }
        }

        private static async Task PrintGroupDetail(DirectoryServices directory, string[] groups)
        {
            try
            {
                Console.WriteLine($"\n{new string('-', 50)}\n");
                // using (var cnn = await directory.GetIDVConnection())
                using (var cnn = await directory.GetActiveDirectoryConnectionAsync(msg => Console.WriteLine(msg)))
                {
                    foreach (var group in groups)
                    {
                        var details =
                            await directory.SearchActiveDirectoryGroupAsync(cnn, group, msg => Console.WriteLine(msg));

                        if (details == null)
                        {
                            Console.WriteLine($"Entry not found for group [{group}].");
                            Console.WriteLine($"\n{new string('-', 50)}\n");
                            continue;
                        }

                        Console.WriteLine(
                            $"Found entry for [{details.Name}]\n{new string('-', 50)}\n{details.Dn}\n");

                        if (details.Members == null || details.Members.Count == 0)
                        {
                            Console.WriteLine("No members found.");
                            continue;
                        }

                        Console.WriteLine("Members:");
                        var keypadding =
                            (int)(Math.Ceiling(details.Members.Max(r => r.Uid?.Length ?? 0 + 3) / 10.0) * 10);
                        var fullnamepadding =
                            (int)(Math.Ceiling(details.Members.Max(r => r.FullName?.Length ?? 0) / 10.0) * 10);

                        foreach (var user in details.Members
                            .OrderBy(r => r.LastName)
                            .ThenBy(r => r.FirstName)
                            .ThenBy(r => r.DistinguishedName))
                            Console.WriteLine(
                                $"\t{user.Uid.PadRight(keypadding)} {(user.FullName ?? "").PadRight(fullnamepadding)}{user.DistinguishedName}");
                        Console.WriteLine($"\n{new string('-', 50)}\n");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    $"An exception occurred getting group details: \n\n\t{e.ToString().Split('\n').FirstOrDefault()}\n");
            }
        }

        private static void PrintLdapEntry(string item, LdapEntry entry)
        {
            Console.WriteLine($"Found entry for [{item}]  {entry.Dn}\n{new string('-', 50)}\n");
            var attrs = GetAttributes(entry);
            // Find the smallest set of 10 columns that would show all attribute keys
            var keypadding = (int)(Math.Ceiling(attrs.Keys.Max(r => r.Length + 3) / 10.0) * 10);
            foreach (var attr in attrs)
            {
                Console.Write($"{attr.Key}:  ".PadRight(keypadding));
                Console.WriteLine(string.Concat(attr.Value.Where(c =>
                    c >= 32 && c <= 128 || c == '\t' || c == '\n')));
            }

            Console.WriteLine($"\n{new string('-', 50)}\n");
        }

        private static Dictionary<string, string> GetAttributes(LdapEntry entry)
        {
            return entry.GetAttributeSet().ToDictionary(kvp => kvp.Key,
                kvp => string.Join("\n\t\t\t", kvp.Value.StringValueArray));
        }

        private static void RunInteractive()
        {
            var login = new LoginView(new LoginViewModel(new DirectoryServices()));
            login.Show();

            if (Credentials.Default.HasCredentials)
            {
                var shell = new ShellView(new ShellViewModel());
                shell.Show();
            }

            Application.Shutdown();
        }
    }
}