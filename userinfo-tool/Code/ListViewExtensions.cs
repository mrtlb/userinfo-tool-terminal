using System;
using System.Collections.Generic;
using System.Linq;
using Terminal.Gui;
using userinfo_tool.Annotations;

namespace userinfo_tool.Code
{
    public static class ListViewExtensions
    {
        public static T[] Items<T>([NotNull] this ListView view)
        {
            if (view.Source == null || view.Source.Count == 0) return Array.Empty<T>();
            return ((List<T>)view.Source.ToList()).ToArray();
        }

        public static IEnumerable<T> MarkedItems<T>([NotNull] this ListView view)
        {
            if (view.Source == null || view.Source.Count == 0) yield break;

            foreach (var idx in Enumerable.Range(0, view.Source.Count))
                if (view.Source.IsMarked(idx))
                    yield return (T)view.Source.ToList()[idx];
        }

        public static T SelectedItem<T>([NotNull] this ListView view)
        {
            if (view.Source == null || view.Source.Count == 0 || view.SelectedItem < 0 ||
                view.SelectedItem >= view.Source.Count) return default;
            return (T)view.Source.ToList()[view.SelectedItem];
        }

        public static async void AddRange<T>([NotNull] this ListView view, params T[] items)
        {
            var lst = (List<T>)view.Source.ToList() ?? new List<T>();
            var marked = view.MarkedItems<T>();
            lst.AddRange(items);
            await view.SetSourceAsync(lst);
            foreach (var m in marked)
            {
                var i = lst.IndexOf(m);
                view.Source.SetMark(i, true);
            }
        }
    }
}