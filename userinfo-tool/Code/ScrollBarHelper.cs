using System;
using Terminal.Gui;
using userinfo_tool.Annotations;

namespace userinfo_tool.Code
{
    public class ScrollBarHelper : ScrollBarView
    {
        private readonly Func<View, int> _getHostHeight;
        private readonly Func<View, int> _getHostLeft;

        private readonly Func<View, int> _getHostTop;
        private readonly Func<View, int> _getHostWidth;
        private readonly Action<View, int> _setHostLeft;
        private readonly Action<View, int> _setHostTop;

        private bool _added;

        public ScrollBarHelper([NotNull] View host,
            Func<View, int> getHostTop,
            Action<View, int> setHostTop,
            Func<View, int> getHostLeft,
            Action<View, int> setHostLeft,
            Func<View, int> getHostHeight,
            Func<View, int> getHostWidth,
            bool isVertical, bool showBothScrollIndicator = true)
            : base(host, isVertical, showBothScrollIndicator)
        {
            _getHostTop = getHostTop;
            _setHostTop = setHostTop;
            _getHostLeft = getHostLeft;
            _setHostLeft = setHostLeft;
            _getHostHeight = getHostHeight;
            _getHostWidth = getHostWidth;
            AddHandlers();
            Refresh();
        }

        public ScrollBarHelper([NotNull] ListView host) : this(host,
            v => host.TopItem,
            (v, idx) => host.TopItem = idx,
            null,
            null,
            v => host.Source?.Count ?? 0,
            null,
            true, false)
        {
        }


        public ScrollBarHelper([NotNull] TextView host) : this(host,
            v => host.TopRow,
            (v, idx) => host.TopRow = idx,
            v => host.LeftColumn,
            (v, idx) => host.LeftColumn = idx,
            v => host.Lines,
            v => host.Frame.Width - 2,
            true, false)
        {
        }

        public ScrollBarHelper([NotNull] TableView host) : this(host,
            v => host.RowOffset,
            (v, idx) => host.RowOffset = idx,
            v => host.ColumnOffset,
            (v, idx) => host.ColumnOffset = idx,
            v => host.Table?.Rows?.Count ?? 0,
            v => host.Table?.Columns?.Count ?? 0,
            true)
        {
        }

        public void AddHandlers()
        {
            if (!_added)
            {
                Host.DrawContent += _hostView_DrawContent;
                ChangedPosition += _scrollBar_ChangedPosition;
                if (OtherScrollBarView != null)
                    OtherScrollBarView.ChangedPosition += _scrollBar_OtherScrollBarView_ChangedPosition;
            }

            _added = true;
        }

        public void RemoveHandlers()
        {
            if (_added)
            {
                Host.DrawContent -= _hostView_DrawContent;
                ChangedPosition -= _scrollBar_ChangedPosition;
                if (OtherScrollBarView != null)
                    OtherScrollBarView.ChangedPosition -= _scrollBar_OtherScrollBarView_ChangedPosition;
            }

            _added = false;
        }

        private void _hostView_DrawContent(Rect obj)
        {
            Size = _getHostHeight(Host);
            Position = _getHostTop(Host);
            if (_getHostWidth != null && OtherScrollBarView != null)
            {
                OtherScrollBarView.Size = _getHostWidth(Host);
                OtherScrollBarView.Position = _getHostLeft(Host);
            }

            Refresh();
        }

        private void _scrollBar_ChangedPosition()
        {
            _setHostTop(Host, Position);
            if (_getHostTop(Host) != Position) Position = _getHostTop(Host);
            Host.SetNeedsDisplay();
        }

        private void _scrollBar_OtherScrollBarView_ChangedPosition()
        {
            _setHostLeft(Host, OtherScrollBarView.Position);
            if (_getHostLeft(Host) != OtherScrollBarView.Position) OtherScrollBarView.Position = _getHostLeft(Host);
            Host.SetNeedsDisplay();
        }
    }
}