using System;
using System.Text;
using Terminal.Gui;

namespace userinfo_tool.Code
{
    public class TerminalUtils
    {
        public static Toplevel MakeTopLevelWrapperIfNeeded(string title)
        {
            if (Application.Top?.Subviews?.Count > 0) return null;
            Application.UseSystemConsole = true;
            Application.Init();
            var win = new Window
            {
                Title = title,
                X = 0,
                Y = 0,
                Width = Dim.Fill(),
                Height = Dim.Fill()
            };

            Application.Top?.Add(win);
            return Application.Top;
        }

        public static string GetPassword()
        {
            var input = new StringBuilder();
            while (true)
            {
                var x = Console.CursorLeft;
                var y = Console.CursorTop;
                var key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    break;
                }

                if (key.Key == ConsoleKey.Backspace && input.Length > 0)
                {
                    input.Remove(input.Length - 1, 1);
                    Console.SetCursorPosition(x - 1, y);
                    Console.Write(" ");
                    Console.SetCursorPosition(x - 1, y);
                }
                else if (key.Key != ConsoleKey.Backspace)
                {
                    input.Append(key.KeyChar);
                    Console.Write("*");
                }
            }

            return input.ToString();
        }
    }
}