using System.ComponentModel;
using System.Runtime.CompilerServices;
using userinfo_tool.Annotations;

namespace userinfo_tool.Models
{
    public class UserName : INotifyPropertyChanged
    {
        private string _distinguishedName;
        private string _firstName;

        private string _fullName;
        private string _lastName;

        private string _uid;

        public string Uid
        {
            get => _uid;
            set
            {
                if (_uid == value) return;
                _uid = value;
                OnPropertyChanged();
            }
        }

        public string FullName
        {
            get => _fullName;
            set
            {
                if (_fullName == value) return;
                _fullName = value;
                OnPropertyChanged();
            }
        }

        public string DistinguishedName
        {
            get => _distinguishedName;
            set
            {
                if (_distinguishedName == value) return;
                _distinguishedName = value;
                OnPropertyChanged();
            }
        }

        public string FirstName
        {
            get => _firstName;
            set
            {
                if (value == _firstName) return;
                _firstName = value;
                OnPropertyChanged();
            }
        }

        public string LastName
        {
            get => _lastName;
            set
            {
                if (value == _lastName) return;
                _lastName = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return $"{_uid,-10} {_fullName}";
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}