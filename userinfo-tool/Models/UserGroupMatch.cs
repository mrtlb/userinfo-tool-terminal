using System.Collections.Generic;

namespace userinfo_tool.Models
{
    public class UserGroupMatch
    {
        public static int NameWidth = 25;
        public string Name { get; set; }
        public string Dn { get; set; }
        public List<string> MarkedUsers { get; set; }

        public override string ToString()
        {
            return $"{Name.PadRight(NameWidth)}  {string.Join(", ", MarkedUsers)}";
        }
    }
}