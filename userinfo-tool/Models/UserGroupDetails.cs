using System.Collections.Generic;

namespace userinfo_tool.Models
{
    public class UserGroupDetails
    {
        public string Name { get; set; }
        public string Dn { get; set; }
        public string Description { get; set; }
        public List<UserName> Members { get; set; }
        public string[] UserDns { get; set; }
    }
}