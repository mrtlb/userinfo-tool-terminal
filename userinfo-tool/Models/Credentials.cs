using System.ComponentModel;
using System.Runtime.CompilerServices;
using userinfo_tool.Annotations;

namespace userinfo_tool.Models
{
    public sealed class Credentials : INotifyPropertyChanged
    {
        private string _password;
        private string _userName;

        private Credentials()
        {
        }

        public static Credentials Default { get; } = new();

        public bool HasCredentials => !(string.IsNullOrWhiteSpace(_userName) || string.IsNullOrWhiteSpace(_password));

        public string UserName
        {
            get => _userName;
            set
            {
                if (_userName == value) return;
                _userName = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(HasCredentials));
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (_password == value) return;
                _password = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(HasCredentials));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}