using System;
using Terminal.Gui;
using userinfo_tool.Models;
using userinfo_tool.ViewModels;
using userinfo_tool.Views.ShellSubviews;

namespace userinfo_tool.Views
{
    public class ShellView
    {
        private readonly ShellViewModel _viewModel;
        private StatusItem _credentialNameStatus;
        private StatusItem _credentialStatus;

        private MenuBar _menu;
        private StatusBar _status;

        public ShellView(ShellViewModel viewModel)
        {
            _viewModel = viewModel;
            Application.UseSystemConsole = true;
            Application.Init();
            //Application.HeightAsBuffer = true;
            //Application.AlwaysSetPosition = true;

            Application.Top.ColorScheme = Colors.Base;

            EntryView = new EntryView(this, viewModel);

            UserListView = new UserListView(this, viewModel);

            LdapDetailsView = new LdapDetailsView(this, viewModel);

            UserGroupView = new UserGroupView(this, viewModel);

            CreateMenu();

            CreateStatusBar();

            Application.Top.KeyPress += Top_KeyPress_Handler;
            Application.Top.Add(EntryView, UserListView, LdapDetailsView, UserGroupView, _menu, _status);
            Application.Top.SetNeedsDisplay();
        }

        public LdapDetailsView LdapDetailsView { get; }

        public UserListView UserListView { get; }

        public EntryView EntryView { get; }

        public UserGroupView UserGroupView { get; }

        #region UI Creation

        public void Show()
        {
            Application.Run(Application.Top);
        }

        private void CreateStatusBar()
        {
            _status = new StatusBar(new[]
            {
                _credentialNameStatus = new StatusItem(Key.Unknown, null, null),
                _credentialStatus = new StatusItem(Key.Unknown, null, null),
                new StatusItem(Key.CharMask, "~^Q~ Quit", null),
                new StatusItem(Key.CharMask, "~^T~ or ~F9~ Menu", null)
            });
            UpdateCredentialsStatus();
        }

        private void UpdateCredentialsStatus()
        {
            _credentialNameStatus.Title = $"User: {Credentials.Default.UserName}";
            _credentialStatus.Title = $"Logged {(Credentials.Default.HasCredentials ? "In" : "Out")}";
            if (UserGroupView.Visible) UserGroupView.ShowUserGroups();
        }

        private void CreateMenu()
        {
            MenuItem toggleLdapViewMenu = null;
            MenuItem toggleUserGroupViewMenu = null;

            void UpdateRadioButtons()
            {
                if (toggleLdapViewMenu != null) toggleLdapViewMenu.Checked = LdapDetailsView.Visible;
                if (toggleUserGroupViewMenu != null) toggleUserGroupViewMenu.Checked = UserGroupView.Visible;
                UserListView.ViewMode = UserGroupView.Visible
                    ? UserListView.UserListViewMode.UserGroups
                    : UserListView.UserListViewMode.Ldap;
                Application.Top.SetNeedsDisplay();
            }

            _menu = new MenuBar(new[]
            {
                new MenuBarItem("_File", new[]
                {
                    new MenuItem("_Clear", "Clear User List", () => { UserListView.ClearList(); },
                        () => UserListView.Users.Length > 0, null, Key.CtrlMask | Key.X),
                    new MenuItem("_Switch", "Change User Credentials", () =>
                    {
                        var login = new LoginView(_viewModel.GetLoginViewModel());
                        login.Authenticated += () =>
                        {
                            UserGroupView.ClearList();
                            UpdateCredentialsStatus();
                        };
                        login.Show();
                    }, null, null, Key.CtrlMask | Key.S),
                    null /* separator */,
                    new MenuItem("_Quit", "Exit Application", () => Application.RequestStop(), null, null,
                        Key.CtrlMask | Key.Q)
                }),
                new MenuBarItem("_View", new[]
                {
                    toggleLdapViewMenu = new MenuItem("_LDAP Detail", null, () =>
                    {
                        LdapDetailsView.Visible = true;
                        UserGroupView.Visible = false;
                        UpdateRadioButtons();
                    }, null, null, Key.CtrlMask | Key.P) { CheckType = MenuItemCheckStyle.Radio },
                    toggleUserGroupViewMenu = new MenuItem("_User Groups", null, () =>
                    {
                        LdapDetailsView.Visible = false;
                        UserGroupView.Visible = true;
                        UpdateRadioButtons();
                    }, null, null, Key.CtrlMask | Key.U) { CheckType = MenuItemCheckStyle.Radio },
                    null /* separator */,
                    new MenuItem("_Toggle Views", null, () =>
                    {
                        LdapDetailsView.Visible ^= true;
                        UserGroupView.Visible ^= true;
                        UpdateRadioButtons();
                    }, null, null, Key.CtrlMask | Key.G)
                })
            });
        }

        private void Top_KeyPress_Handler(View.KeyEventEventArgs e)
        {
            switch (ShortcutHelper.GetModifiersKey(e.KeyEvent))
            {
                case Key.CtrlMask | Key.T:
                    if (_menu.IsMenuOpen)
                        _menu.CloseMenu();
                    else
                        _menu.OpenMenu();
                    e.Handled = true;
                    break;
            }
        }

        public void SetStatusItem(StatusItem item)
        {
            var idx = Array.IndexOf(_status.Items, item);
            if (idx == -1) _status.AddItemAt(_status.Items.Length, item);
        }

        public void RemoveStatusItem(StatusItem item)
        {
            var idx = Array.IndexOf(_status.Items, item);
            if (idx >= 0) _status.RemoveItem(idx);
        }

        #endregion
    }
}