using System;
using System.Linq;
using Terminal.Gui;
using userinfo_tool.Code;
using userinfo_tool.Models;
using userinfo_tool.ViewModels;

namespace userinfo_tool.Views
{
    public class FoundUsersView
    {
        private readonly FoundUsersViewModel _viewModel;
        private Dialog _dialog;

        public FoundUsersView(FoundUsersViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public void Show()
        {
            Button okBtn;

            var listView = new ListView(_viewModel.UserNames
                .OrderBy(u => u.LastName)
                .ThenBy(u => u.FirstName)
                .ThenBy(u => u.Uid)
                .ToList())
            {
                AllowsMultipleSelection = true,
                AllowsMarking = true,
                Width = Dim.Sized(_viewModel.UserNames.Max(r => r.ToString().Length)),
                Height = Dim.Fill() - 1
            };

            _dialog = new Dialog("Found Names",
                okBtn = new Button("_Ok", true))
            {
                Width = Dim.Width(listView) + 4,
                Height = Dim.Percent(50)
            };
            _viewModel.Cancelled = true;

            okBtn.Clicked += () =>
            {
                _viewModel.Cancelled = false;
                _viewModel.SelectedUserNames.Clear();
                _viewModel.SelectedUserNames.AddRange(listView.MarkedItems<UserName>());
                Application.RequestStop();
            };
            _dialog.Add(listView);
            _dialog.Closed += _ => Closed?.Invoke(_viewModel);
            Application.Run(_dialog);
        }

        public event Action<FoundUsersViewModel> Closed;
    }
}