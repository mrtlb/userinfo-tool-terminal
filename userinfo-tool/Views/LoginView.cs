using System;
using Terminal.Gui;
using userinfo_tool.Code;
using userinfo_tool.ViewModels;

namespace userinfo_tool.Views
{
    public class LoginView
    {
        private readonly LoginViewModel _viewModel;
        private bool _cancelling;

        public LoginView(LoginViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public void Show()
        {
            var win = TerminalUtils.MakeTopLevelWrapperIfNeeded("UserInfo Tool");

            var nameLbl = new Label("Name: ") { X = Pos.Percent(30) - 6, Y = 1 };
            var nameTxt = new TextField
            {
                X = Pos.Right(nameLbl),
                Y = Pos.Top(nameLbl),
                Width = Dim.Percent(50)
            };
            nameTxt.TextChanged += _ => _viewModel.UserName = nameTxt.Text.ToString();

            var pwLbl = new Label("Password: ") { X = Pos.Percent(30) - 10, Y = Pos.Bottom(nameLbl) + 1 };
            var pwTxt = new TextField
            {
                X = Pos.Right(pwLbl),
                Y = Pos.Top(pwLbl),
                Width = Dim.Percent(50),
                Secret = true
            };
            pwTxt.TextChanged += _ => _viewModel.Password = pwTxt.Text.ToString();

            //Need to capture before closure is lost
            var _authenticated = Authenticated;
            var ok = new Button("_Ok", true);
            ok.Clicked += async () =>
            {
                _cancelling = false;
                if (_cancelling) return;

                if (string.IsNullOrWhiteSpace(_viewModel.Password))
                {
                    MessageBox.ErrorQuery("Error", "Password required to log in.", "_Ok");
                    return;
                }

                var wait = new Dialog("Authenticating")
                {
                    ColorScheme = Colors.Error,
                    Width = Dim.Percent(20),
                    Height = 3
                };
                wait.Add(new Label("Please wait.") { X = Pos.Center(), Y = 0 });

                win.Add(wait);
                var authenticated = await _viewModel.Authenticate();

                win.Remove(wait);
                if (!authenticated)
                {
                    MessageBox.ErrorQuery("Invalid Login", "Unable to authenticate the credentials provided.",
                        "_Ok");
                    nameTxt.Text = "";
                    pwTxt.Text = "";
                    nameTxt.SetFocus();
                }
                else
                {
                    _authenticated?.Invoke();
                    Application.RequestStop();
                }
            };

            var cancel = new Button("_Cancel");
            cancel.Clicked += () =>
            {
                _viewModel.UserName = null;
                _viewModel.Password = null;
                _cancelling = true;
                Application.RequestStop();
            };

            var dialog = new Dialog("Login",
                ok, cancel)
            {
                Width = Dim.Percent(75),
                Height = 8
            };
            win ??= dialog;

            win.Loaded += () => nameTxt.SetFocus();
            dialog.Add(nameLbl, nameTxt, pwLbl, pwTxt);

            Application.Top.Add(dialog);
            Application.Run(win);
            Application.Top.Remove(dialog);
        }

        public event Action Authenticated;
    }
}