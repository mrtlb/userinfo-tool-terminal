using System;
using System.Data;
using System.Linq;
using Terminal.Gui;
using userinfo_tool.Code;
using userinfo_tool.ViewModels;

namespace userinfo_tool.Views.ShellSubviews
{
    public class LdapDetailsView : FrameView
    {
        private readonly TableView _detailTable;
        private readonly ShellViewModel _viewModel;
        private readonly ShellView _shellView;


        private string lastUserId;

        public LdapDetailsView(ShellView shellView, ShellViewModel viewModel) : base("LDAP Info")
        {
            _shellView = shellView;
            _viewModel = viewModel;
            X = Pos.Right(_shellView.UserListView);
            Y = 1;
            Width = Dim.Fill();
            Height = Dim.Fill(1);

            _detailTable = new TableView
            {
                Width = Dim.Fill(),
                Height = Dim.Fill(),
                Style = new TableView.TableStyle
                {
                    ShowHorizontalHeaderOverline = false,
                    ShowHorizontalHeaderUnderline = false,
                    ShowVerticalCellLines = false,
                    ShowVerticalHeaderLines = false
                }
            };
            _detailTable.DrawContent += rect => { _detailTable.MaxCellWidth = rect.Width / 2 - 1; };
            Add(_detailTable);
            VisibleChanged += () =>
            {
                if (Visible)
                {
                    if (_shellView.UserListView.Users.Length == 0)
                        _detailTable.Table = null;
                    else
                        ShowLdapDetail(_shellView.UserListView.SelectedUser?.Uid);
                }
            };
            // ReSharper disable once ObjectCreationAsStatement
#pragma warning disable CA1806
            new ScrollBarHelper(_detailTable);
#pragma warning restore CA1806
        }

        public async void ShowLdapDetail(string userId)
        {
            lastUserId = userId;
            if (string.IsNullOrWhiteSpace(userId))
            {
                ClearDetail();
                return;
            }

            if (!Visible) return;

            var detail = await _viewModel.GetLdapDetail(userId);

            //If things have already changed, don't change them again
            if (lastUserId != userId) return;

            var dt = new DataTable();
            dt.Columns.AddRange(new[] { new DataColumn("Name"), new DataColumn("Value") });
            foreach (var (key, value) in detail)
            {
                var values = value?.Split('\n',
                    StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
                var bFirst = true;
                if (values == null) continue;
                foreach (var val in values)
                {
                    dt.Rows.Add(bFirst ? key : "",
                        string.Concat(val.Where(c => c >= 32 && c <= 128 || c == '\t')));
                    bFirst = false;
                }
            }

            _detailTable.Table = dt;
            _detailTable.Update();
        }

        public void ClearDetail()
        {
            lastUserId = null;
            _detailTable.Table = null;
        }
    }
}