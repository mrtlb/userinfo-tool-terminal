using System.Linq;
using Novell.Directory.Ldap;
using Terminal.Gui;
using userinfo_tool.ViewModels;

namespace userinfo_tool.Views.ShellSubviews
{
    public class EntryView : View
    {
        private readonly TextField _entryTxt;
        private readonly ShellView _shellView;
        private readonly ShellViewModel _viewModel;

        public EntryView(ShellView shellView, ShellViewModel _viewModel)
        {
            X = 0;
            Y = 1;
            Width = Dim.Percent(20);
            Height = 1;

            _shellView = shellView;
            this._viewModel = _viewModel;


            Label entryLbl;
            var resolveNamesItem =
                new StatusItem(Key.CtrlMask | Key.R, "~^R~ Resolve Names", ResolveNames);
            Add(
                entryLbl = new Label("Enter: ") { X = 0, Y = 0 },
                _entryTxt = new TextField
                {
                    X = Pos.Right(entryLbl) + 1,
                    Y = Pos.Top(entryLbl),
                    Width = Dim.Fill() - 2,
                    Height = 1
                });
            _entryTxt.KeyPress += args =>
            {
                if (args.KeyEvent.Key != Key.Enter) return;
                _shellView.UserListView.AddUsers(Names);
                Names = "";
                args.Handled = true;
            };
            _entryTxt.Enter += _ => _shellView.SetStatusItem(resolveNamesItem);
            _entryTxt.Leave += _ => _shellView.RemoveStatusItem(resolveNamesItem);
        }

        public string Names
        {
            get => _entryTxt.Text?.ToString();
            set => _entryTxt.Text = value;
        }

        public new void SetFocus()
        {
            _entryTxt.SetFocus();
        }

        public async void ResolveNames()
        {
            try
            {
                var names = await _viewModel.ResolveNames(Names);
                if (names?.Count <= 0) return;

                var vm = new FoundUsersViewModel(names);
                var v = new FoundUsersView(vm);
                v.Closed += vm =>
                {
                    if (!vm.Cancelled) Names = string.Join(", ", vm.SelectedUserNames.Select(u => u.Uid));
                };
                v.Show();
            }
            catch (LdapException e)
            {
                MessageBox.Query("Error",
                    $"{e.GetType().Name}: {e.Message}\n\nA search term returned more than 1000 results. Try being more specific.\n\n",
                    0, "_Ok");
            }
        }
    }
}