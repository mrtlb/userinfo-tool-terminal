using System;
using System.Linq;
using System.Text;
using Terminal.Gui;
using userinfo_tool.Code;
using userinfo_tool.Models;
using userinfo_tool.ViewModels;

namespace userinfo_tool.Views.ShellSubviews
{
    public class UserGroupView : FrameView
    {
        private readonly ShellView _shellView;
        private readonly ListView _userGroupList;
        private readonly StatusItem _userGroupMessageStatusBarItem;
        private readonly ShellViewModel _viewModel;
        private string[] _lastUserGroupNameUids;

        public UserGroupView(ShellView shellView, ShellViewModel viewModel) : base("User Groups")
        {
            _shellView = shellView;
            _viewModel = viewModel;
            _userGroupMessageStatusBarItem = new StatusItem(Key.CharMask, "", null);
            X = Pos.Right(_shellView.UserListView);
            Y = 1;
            Width = Dim.Fill();
            Height = Dim.Fill(1);
            Visible = false;

            VisibleChanged += () =>
            {
                if (Visible)
                {
                    _shellView.SetStatusItem(_userGroupMessageStatusBarItem);
                    ShowUserGroups();
                }
                else
                {
                    _shellView.RemoveStatusItem(_userGroupMessageStatusBarItem);
                }
            };
            _userGroupList = new ListView
            {
                Width = Dim.Fill(),
                Height = Dim.Fill()
            };
            _userGroupList.OpenSelectedItem += async args =>
            {
                var ok = new Button("_Ok", true);
                var dialog = new Dialog("Group Detail: ", ok);
                ok.Clicked += () => Application.RequestStop(dialog);
                var txt = new TextView
                {
                    Multiline = true,
                    WordWrap = true,
                    Width = Dim.Fill(),
                    Height = Dim.Fill() - 1
                };
                dialog.Add(txt);
                new ScrollBarHelper(txt);

                var sb = new StringBuilder();
                var details =
                    await _viewModel.GetGroupDetail((args.Value as UserGroupMatch).Name, msg => sb.AppendLine(msg));
                sb.AppendLine($"{new string('-', 50)}\nGroup:   {details.Dn}\n\nMember:");

                var keypadding = (int)(Math.Ceiling(details.Members.Max(r => r.Uid?.Length ?? 0) / 10.0) * 10)+3;
                var fullnamepadding =
                    (int)(Math.Ceiling(details.Members.Max(r => r.FullName?.Length ?? 0) / 10.0) * 10)+3;

                foreach (var user in details.Members
                    .OrderBy(r => r.LastName)
                    .ThenBy(r => r.FirstName)
                    .ThenBy(r => r.DistinguishedName))
                    sb.AppendLine(
                        $"\t{user.Uid.PadRight(keypadding)} {(user.FullName ?? "").PadRight(fullnamepadding)}{user.DistinguishedName}");

                Application.MainLoop.Invoke(() => txt.Text += sb.ToString());
                Application.Run(dialog);
            };
            Add(_userGroupList);
            // ReSharper disable once ObjectCreationAsStatement
#pragma warning disable CA1806
            new ScrollBarHelper(_userGroupList);
#pragma warning restore CA1806
        }


        public async void ShowUserGroups()
        {
            var users = _shellView.UserListView.Users;
            if (users.Length == 0)
            {
                _userGroupList.Source = null;
                return;
            }

            if (SameUserGroupNames(out var nameUids)) return;

            _userGroupList.Source = null;

            var lst = await _viewModel.GetUserGroups(nameUids,
                    msg => _userGroupMessageStatusBarItem.Title = string.Concat(
                        msg.Where(c => c >= 32 && c <= 128 || c is '\t' or '\n')))
                .ToListAsync();
            if (lst is { Count: > 0 })
                UserGroupMatch.NameWidth = (int)Math.Ceiling((lst.Max(r => r.Name.Length) + 3) / 10.0) * 10;

            await _userGroupList.SetSourceAsync(lst);
        }

        private bool SameUserGroupNames(out string[] nameUids)
        {
            nameUids = _shellView.UserListView.MarkedUsers
                .Select(u => u.Uid)
                .OrderBy(r => r)
                .ToArray();
            if (nameUids.Length == 0)
                nameUids = new[] { _shellView.UserListView.SelectedUser?.Uid };

            //Skip the lookup if this is the same list
            if (_lastUserGroupNameUids != null && _lastUserGroupNameUids.SequenceEqual(nameUids)) return true;
            _lastUserGroupNameUids = nameUids;
            return false;
        }

        public void ClearList()
        {
            _userGroupList.Source = null;
            _lastUserGroupNameUids = null;
        }
    }
}