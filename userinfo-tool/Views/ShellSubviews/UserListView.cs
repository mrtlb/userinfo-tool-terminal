using System;
using System.Collections.Generic;
using System.Linq;
using Terminal.Gui;
using userinfo_tool.Code;
using userinfo_tool.Models;
using userinfo_tool.ViewModels;

namespace userinfo_tool.Views.ShellSubviews
{
    public class UserListView : FrameView
    {
        public enum UserListViewMode
        {
            Ldap,
            UserGroups
        }

        private readonly ShellView _shellView;
        private readonly ListView _userList;
        private readonly ShellViewModel _viewModel;
        private UserListViewMode _viewMode = UserListViewMode.Ldap;

        public UserListView(ShellView shellView, ShellViewModel viewModel) : base("Users")
        {
            _shellView = shellView;
            _viewModel = viewModel;
            var removeUserStatusItem =
                new StatusItem(Key.CharMask, "~Del/Backspace~ Remove User", null);

            X = 0;
            Y = Pos.Bottom(_shellView.EntryView);
            Width = Dim.Percent(20);
            Height = Dim.Fill(1);

            _userList = new ListView
            {
                Width = Dim.Fill(),
                Height = Dim.Fill()
            };
            Add(_userList);
            // ReSharper disable once ObjectCreationAsStatement
#pragma warning disable CA1806
            new ScrollBarHelper(_userList);
#pragma warning restore CA1806

            void updateSelections(string uid)
            {
                if (_shellView.LdapDetailsView.Visible)
                    _shellView.LdapDetailsView.ShowLdapDetail(uid);
                else
                    _shellView.UserGroupView.ShowUserGroups();
            }

            _userList.SelectedItemChanged += args =>
            {
                if (args.Value is not UserName name) return;
                updateSelections(name.Uid);
            };
            _userList.MouseClick += args =>
            {
                if (_shellView.UserGroupView.Visible)
                {
                    var name = _userList.SelectedItem<UserName>();
                    if (name == null) return;
                    Application.MainLoop.Invoke(() => updateSelections(name.Uid));
                }
            };
            _userList.KeyPress += args =>
            {
                if (args.KeyEvent.Key is Key.Space && _shellView.UserGroupView.Visible)
                {
                    var name = _userList.SelectedItem<UserName>();
                    if (name == null) return;
                    Application.MainLoop.Invoke(() => updateSelections(name.Uid));
                }

                if (args.KeyEvent.Key is not (Key.Delete or Key.DeleteChar or Key.Backspace)) return;

                var lst = _userList.Source?.ToList();
                if (lst == null || lst.Count == 0) return;

                lst.RemoveAt(_userList.SelectedItem);
                if (_userList.SelectedItem >= lst.Count) _userList.SelectedItem = lst.Count - 1;

                _userList.SetNeedsDisplay();
                if (lst.Count > 0)
                {
                    if (_shellView.LdapDetailsView.Visible)
                        _shellView.LdapDetailsView.ShowLdapDetail(_userList.SelectedItem<UserName>()?.Uid);
                    else
                        _shellView.UserGroupView.ShowUserGroups();
                }
                else
                {
                    if (_shellView.LdapDetailsView.Visible)
                        _shellView.LdapDetailsView.ClearDetail();
                    else
                        _shellView.UserGroupView.ClearList();

                    _shellView.EntryView.SetFocus();
                }

                args.Handled = true;
            };
            Enter += _ => _shellView.SetStatusItem(removeUserStatusItem);
            Leave += _ => _shellView.RemoveStatusItem(removeUserStatusItem);
        }

        public UserName[] Users => _userList.Items<UserName>();
        public UserName SelectedUser => _userList.SelectedItem<UserName>();

        public IEnumerable<UserName> MarkedUsers => _userList.MarkedItems<UserName>();

        public UserListViewMode ViewMode
        {
            get => _viewMode;
            set
            {
                _viewMode = value;
                _userList.AllowsMarking = _viewMode == UserListViewMode.UserGroups;
                AdjustUserListSize();
            }
        }

        public async void AddUsers(string names)
        {
            var users = await _viewModel.ResolveNames(names);
            if (users == null || users.Count == 0) return;

            if (_userList.Source == null)
            {
                await _userList.SetSourceAsync(users);
                _userList.SelectedItem = 0;
                _shellView.UserGroupView.ShowUserGroups();
            }
            else
            {
                _userList.AddRange(users.ToArray());
            }

            AdjustUserListSize();
            Application.Top.SetChildNeedsDisplay();
        }

        private void AdjustUserListSize()
        {
            if (_userList.Items<UserName>() is { Length: > 0 } lst)
            {
                var width = Math.Max(
                    lst.Max(u => u.ToString().Length +
                                 2 +
                                 (_userList.AllowsMarking ? 2 : 0) +
                                 (_userList.Bounds.Height < lst.Length ? 1 : 0)),
                    25);
                LayoutStyle = LayoutStyle.Absolute;
                Width = Dim.Sized(width);
                LayoutStyle = LayoutStyle.Computed;
            }
            else
            {
                Width = Dim.Percent(20);
            }
        }

        public void ClearList()
        {
            _userList.Source = null;
            _shellView.EntryView.SetFocus();
            if (_shellView.LdapDetailsView.Visible)
                _shellView.LdapDetailsView.ShowLdapDetail(null);
            else
                _shellView.UserGroupView.ShowUserGroups();
        }
    }
}